package com.example.resouceserver.service;

import com.example.resouceserver.dto.PajakDto;
import com.example.resouceserver.entity.PajakEntity;
import com.example.resouceserver.exceptions.IdNotFoundException;
import com.example.resouceserver.repository.PajakRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PajakService {

    private final PajakRepository pajakRepository;

    public PajakEntity save(PajakDto.Save data){
        UUID noResi = UUID.randomUUID();
        PajakEntity entity = new PajakEntity(null, noResi.toString(), data.getName(), data.getNpwp(), new Timestamp(System.currentTimeMillis()), "created");
        return pajakRepository.save(entity);
    }

    public List<PajakEntity> listPajak(){
        return pajakRepository.findAll();
    }

    public List<PajakEntity> listPajakApprove(){
        return pajakRepository.findByStatus("approve");
    }

    public PajakEntity findByNoResi(Long noResi){
        return pajakRepository.findByNoResi(noResi).orElseThrow(() ->
            new IdNotFoundException("Pajak dengan nomor resi " + noResi + " tidak ditemukan")
        );
    }

    public int update(String status, Long id){
        PajakEntity entity = pajakRepository.findById(id).orElseThrow(() ->
                new IdNotFoundException("Pajak dengan id " + id + " tidak ditemukan")
        );
        return pajakRepository.updateStatus(status, id);
    }

}
