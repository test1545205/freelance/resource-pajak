package com.example.resouceserver.repository;

import com.example.resouceserver.entity.PajakEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PajakRepository extends JpaRepository<PajakEntity, Long> {
    Optional<PajakEntity> findByNoResi(Long noResi);
    List<PajakEntity> findByStatus(String status);

    @Modifying
    @Transactional
    @Query(value = """
                UPDATE public.tm_pajak
                SET status=:status
                WHERE id=:id
            """, nativeQuery = true)
    int updateStatus(@Param("status") String status, @Param("id") Long id);
}
