package com.example.resouceserver.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        System.out.println("========== masukk ====> ");
        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(new Converter<Jwt, Collection<GrantedAuthority>>() {
            @Override
            public Collection<GrantedAuthority> convert(Jwt jwt) {
                @SuppressWarnings("unchecked")
                Collection<String> roles = (ArrayList) jwt.getClaims().get("roles");
                System.out.println("========== claims ====> " + roles.toString());
                if (roles == null || roles.isEmpty()) {
                    return new ArrayList<>();
                }
                return roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
            }
        });
        http
                .csrf().disable()
                .authorizeHttpRequests(authorize ->
                        {
                            try {
//                                authorize.anyRequest().hasAuthority("SCOPE_message.read")
                                authorize
//                                        .anyRequest().authenticated()
                                        .requestMatchers(HttpMethod.GET, "/api/pajak").hasRole("CHECKER")
                                        .requestMatchers(HttpMethod.POST, "/api/pajak").hasRole("MAKER")
//                                        .requestMatchers("/api/home/admin").hasRole("ADMIN")
                                    .and()
                                    .oauth2ResourceServer()
                                    .jwt()
                                    .jwtAuthenticationConverter(jwtAuthenticationConverter);
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                )
                .httpBasic().disable()
                .formLogin().disable();
        return http.build();

    }

}
