package com.example.resouceserver.controller;

import com.example.resouceserver.dto.PajakDto;
import com.example.resouceserver.entity.PajakEntity;
import com.example.resouceserver.service.PajakService;
import jakarta.annotation.security.RolesAllowed;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pajak")
public class PajakController {

    private final PajakService service;

    @PostMapping
//    @RolesAllowed("MAKER")
    public ResponseEntity<PajakEntity> save(@RequestBody PajakDto.Save data){
        return ResponseEntity.ok(service.save(data));
    }

    @GetMapping
//    @PreAuthorize("hasRole('ROLE_CHECKER')")
    public ResponseEntity<List<PajakEntity>> listAll(){
        return ResponseEntity.ok(service.listPajak());
    }

    @PutMapping
//    @PreAuthorize("hasRole('CHECKER')")
    public ResponseEntity<String> updateStatus(
            @RequestParam(name = "status") String status,
            @RequestParam(name = "id") Long id
    ){
        int updatedRows = service.update(status, id);
        if (updatedRows > 0){
            return ResponseEntity.ok("status berhasil diupdate menjadi " + status);
        }

        return ResponseEntity.ok("update gagal");
    }

    @GetMapping("/list-approve")
//    @PreAuthorize("hasRole('APPROVER')")
    public ResponseEntity<List<PajakEntity>> listPajakApprove(){
        return ResponseEntity.ok(service.listPajakApprove());
    }

}
