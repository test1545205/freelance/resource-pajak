package com.example.resouceserver.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class PajakDto {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Save{
        private String name;
        private Long npwp;
    }

}
